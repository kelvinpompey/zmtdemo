import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { StackNavigator } from 'react-navigation';
import { Provider } from 'mobx-react';
import { inject, observer } from "mobx-react/native";
import { userStore } from './stores'; 
import { HomeScreen, SignUpScreen } from './screens'; 

@observer
export default class App extends React.Component {
  componentDidMount() {
  }

  render() {
    return (
      <Provider userStore={userStore}>        
        {userStore.user ? <HomeScreen /> : <SignUpScreen />}
      </Provider>
    );
  }
}
