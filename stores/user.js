
import {observable, action} from 'mobx'

class UserStore {
    @observable user = null 

    @action signUp() {
        this.user = {name: "Kelvin", authenticated: true}
        console.log('signing up ', this.user)
    }

    @action logout() {
        this.user = null 
    }

    userName() {
        return this.user ? this.user.name : 'Anonymous'; 
    }
}

export default new UserStore; 