import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { inject, observer } from "mobx-react/native";
import { Constants } from "expo"; 

@inject('userStore') @observer
class Home extends React.Component { 
    static navigationOptions = {
        title: 'Zip Dev',
        headerTintColor: "#ffffff",        
        headerStyle: {
            marginTop: Constants.statusBarHeight, 
            backgroundColor: "purple"
        }           
    }

    componentDidMount() {
    }
    render() {
        return (
        <View style={styles.container}>
            <Text>Home Screen for {this.props.userStore.userName()}</Text>
            <Button title="logout" onPress={this.logout.bind(this)}></Button>
        </View>)    
    }

    logout() {
        this.props.userStore.logout()
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#282425', 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'flex-start'         
    }
})

const stack = StackNavigator({
    Home: {
      screen: Home,
    }
});

export default stack; 