import React from 'react';
import { StyleSheet, Text, View, ScrollView, Button, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { inject, observer } from "mobx-react/native";
import { Constants } from "expo"; 
import IconTextInput from '../components/IconTextInput'; 
import ValidationComponent from 'react-native-form-validator';
import { Ionicons, MaterialIcons } from '@expo/vector-icons'; 
import GradientButton from '../components/GradientButton'; 
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

@inject('userStore') @observer
export default class SignUp extends ValidationComponent {

    static navigationOptions = {
        headerTintColor: "#ffffff",
        title: 'Create Developer Account',
        headerStyle: {
            marginTop: Constants.statusBarHeight, 
            backgroundColor: "#8E359E"
        }
    }    

    constructor(props) {
        super(props); 

        this.inputs = []; 
        this.state = {
            email: '',
            firstName: '', 
            lastName: '', 
            phone: '', 
            password: '', 
            confirmPassword: '', 
            errors: { 
                firstName: [], 
                lastName: [], 
                email: [], 
                phone: [], 
                password: [], 
                confirmPassword: [] 
            }
        }

        this.messages = {
            en: {
                numbers: 'Must be a valid number.',
                email: 'Must be a valid email address.',
                required: 'Is mandatory.',
                date: 'Must be a valid date ({1}).',
                minlength: 'Length must be greater than {1}.',
                maxlength: 'Length must be lower than {1}.'                
            }
        }
        this.nextField = this.nextField.bind(this); 
    }

    componentDidMount() {
    }

    dismissKeyboard() {
        Keyboard.dismiss(); 
    }

    render() {
        return (
        <TouchableWithoutFeedback onPress={this.dismissKeyboard.bind(this)} >
            <KeyboardAwareScrollView enableOnAndroid={true} style={styles.scrollView} contentContainerStyle={styles.container}>
            <Text style={[styles.whiteText, styles.bigText]}>Basic Information</Text>
            <IconTextInput
                hasErrors = {this.state.errors['firstName'].length > 0} 
                onChangeText = {(text) => this.setState({firstName: text})}
                childInput ={(input) => {
                    this.inputs['firstName'] = input
                }}
                returnKeyType = {"next"}
                blurOnSubmit = {true}
                title = {"First Name"}
                onSubmitEditing={() => this.nextField("lastName") }
                icon={<MaterialIcons name="person-outline" size={32} color="#ccc" />}
                borderColor={"#9686F8"} />

            <View>{this.state.errors['firstName'].map((error, index)  => <Text style={styles.error} key={index}>{error}</Text>)}</View>

            <IconTextInput 
                hasErrors = {this.state.errors['lastName'].length > 0} 
                onChangeText = {(text) => this.setState({lastName: text})}
                childInput={(input) => {
                    this.inputs['lastName'] = input
                }}            
                returnKeyType={"next"}
                blurOnSubmit={true}
                title={"Last Name"}
                onSubmitEditing={() => this.nextField("email") }                
                icon={<MaterialIcons name="person-outline" size={32} color="#ccc" />}
                borderColor={"#9686F8"} />  

            <View>{this.state.errors['lastName'].map((error, index) => <Text style={styles.error} key={index}>{error}</Text>)}</View>

            <IconTextInput 
                hasErrors = {this.state.errors['email'].length > 0} 
                onChangeText = {(text) => this.setState({email: text})}            
                childInput={(input) => {
                    this.inputs['email'] = input
                }}            
                returnKeyType={"next"}
                blurOnSubmit={false}                
                title={"Email"}
                onSubmitEditing={() => this.nextField("phone")}                
                icon={<MaterialIcons name="mail-outline" size={32} color="#ccc" />}
                borderColor={"#9686F8"} /> 

            <View>{this.state.errors['email'].map((error, index) => <Text key={index} style={styles.error}>{error}</Text>)}</View>

            <IconTextInput 
                hasErrors = {this.state.errors['phone'].length > 0} 
                onChangeText = {(text) => this.setState({phone: text}) }            
                childInput = {(input) => {
                    this.inputs['phone'] = input
                }}            
                returnKeyType = {"next"}
                blurOnSubmit = {false}                
                title = {"Phone"}
                onSubmitEditing = {() => this.nextField("password")}                
                icon = {<MaterialIcons name="phone" size={32} color="#ccc" />}
                borderColor = {"#9686F8"} /> 

            <View>{this.state.errors['phone'].map((error, index) => <Text key={index} style={styles.error}>{error}</Text>)}</View>

            <IconTextInput 
                hasErrors = {this.state.errors['password'].length > 0} 
                onChangeText = {(text) => this.setState({password: text})}            
                childInput={(input) => {
                    this.inputs['password'] = input
                }}            
                returnKeyType={"next"}
                blurOnSubmit={false}                
                title={"Password"}
                onSubmitEditing={() => this.nextField("confirmPassword")}                
                icon={<MaterialIcons name="lock-outline" size={32} color="#ccc" />}
                borderColor={"#9686F8"} /> 

            <View>{this.state.errors['password'].map((error, index) => <Text key={index} style={styles.error}>{error}</Text>)}</View>

            <IconTextInput 
                hasErrors = {this.state.errors['confirmPassword'].length > 0} 
                onChangeText = {(text) => this.setState({confirmPassword: text})}            
                childInput={(input) => {
                    this.inputs['confirmPassword'] = input
                }}            
                returnKeyType={"next"}
                blurOnSubmit={false}                
                title={"Confirm Password"}
                onSubmitEditing={() => this.nextField("confirmPassword")}                
                icon={<MaterialIcons name="lock-outline" size={32} color="#ccc" />}
                borderColor={"#9686F8"} /> 

            <View>{this.state.errors['confirmPassword'].map((error, index) => <Text key={index} style={styles.error}>{error}</Text>)}</View>
                                 

            <GradientButton 
                borderColor={"#9686F8"}
                title="Confirm" 
                onPress={this.submitForm.bind(this)}></GradientButton>
            </KeyboardAwareScrollView>
        </TouchableWithoutFeedback>)    
    }

    signUp() {
        this.props.userStore.signUp(); 
    }

    nextField(field) {
        this.inputs[field].focus(); 
    }
    
    submitForm() {
        this.validate({
            firstName: {minlength:3, required: true},
            lastName: {minlength:3, required: true},      
            phone: {required: true, minlength: 10, numbers: true},      
            email: {email: true}, 
            password: {required: true}, 
            confirmPassword: {required: true}
        });

        let errorMap = {}; 
        ['firstName', 'lastName', 'email', 'phone', 'password', 'confirmPassword'].forEach(key => {
            const errors = this.getErrorsInField(key); 
            errorMap[key] = errors; 
        }); 

        if(this.state.password !== this.state.confirmPassword) {
            errorMap['confirmPassword'].push('Passwords must be the identical'); 
        }
        else {
            errorMap['confirmPassword'] = [];          
        }

        this.setState({'errors': errorMap});
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#282425',         
        alignItems: 'center', 
        justifyContent: 'flex-start', 
    }, 
    scrollView: {
        backgroundColor: '#282425'     
    },
    whiteText: {
        color: 'white'
    }, 
    bigText: {
        marginVertical: 20, 
        fontSize: 24
    }, 
    bold: {
        fontWeight: "400"
    }, 
    error: {
        alignSelf: 'flex-start',
        width: 350, 
        padding: 10, 
        paddingHorizontal: 0, 
        paddingVertical: 5, 
        color: '#f26762'
    }   
})

