import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';
import RoundButton from '../components/RoundButton'; 
import { StackNavigator } from 'react-navigation';
import { inject, observer } from "mobx-react/native";
import { Constants } from "expo"; 
import Step0Screen  from "./Step0"; 

@inject('userStore') @observer
class SignUp extends React.Component {
    static navigationOptions = {
        headerTintColor: "#ffffff",
        header: null, 
        headerStyle: {
            marginTop: Constants.statusBarHeight, 
            backgroundColor: "#8E359E"
        }
    }    
    componentDidMount() {
    }
    render() {
        return (
        <View style={styles.container}>
            <Text style={[styles.whiteText, styles.bigText]}>Create an Account</Text>
            <RoundButton 
                title="I need Developers" 
                borderColor={"#75EAB8"}                
                onPress={() => alert("To be implemented")}>
            </RoundButton>

            <RoundButton 
                title="I'm a Developer" 
                borderColor={"#9686F8"}
                onPress={this.step0.bind(this)}>
            </RoundButton>

            <Text style={[styles.whiteText, styles.bigText]}>&#8213; or &#8213;</Text>

            <RoundButton 
                title="Confirm" 
                borderColor={"transparent"}
                onPress={() => alert("To be implemented")}></RoundButton>

        </View>)    
    }

    signUp() {
        this.props.userStore.signUp(); 
    }

    step0() {
        this.props.navigation.navigate('Step0'); 
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#282425', 
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'flex-start'         
    }, 
    whiteText: {
        color: 'white'
    }, 
    bigText: {
        marginVertical: 20, 
        fontSize: 24
    }, 
    bold: {
        fontWeight: "400"
    }
})

const stack = StackNavigator({
    Home: {
      screen: SignUp,
    }, 
    Step0: {
        screen: Step0Screen
    }
});

export default stack; 

