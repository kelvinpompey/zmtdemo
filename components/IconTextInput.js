import React, { Component } from 'react';
import { StyleSheet, TextInput, View, Button, TouchableOpacity } from 'react-native';

export default class RoundButton extends  Component {
    state = {selected: false} 

    render() {
        return (<View 
                    style={[styles.container, {borderColor: this.border()}]}
                    onPress={this.props.onPress}>   
                {this.props.icon}
                <TextInput 
                    onChangeText={this.props.onChangeText}
                    ref={(input) => this.props.childInput(input)}
                    returnKeyType={this.props.returnKeyType}
                    onFocus={this.focused.bind(this)}
                    blurOnSubmit={this.props.blurOnSubmit}
                    onEndEditing={this.finishedEditing.bind(this)}
                    onSubmitEditing={this.props.onSubmitEditing}
                    underlineColorAndroid={"transparent"}
                    placeholderTextColor={"#ccc"}
                    placeholder={this.props.title} 
                    style={styles.input}></TextInput>          
        </View>)
    }

    border() {
        if(this.props.hasErrors) {
            return '#f26762'; 
        }
        else if(this.state.selected === true) {
            return this.props.borderColor;             
        }
        else {
            return 'transparent'; 
        }
    }

    focused() {
        console.log('focused')
        this.setState({selected: true}); 
    }

    finishedEditing() {
        console.log('finished editing')
        this.setState({selected: false}); 
        
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        padding: 15, 
        paddingHorizontal: 10, 
        borderWidth: 2, 
        marginVertical: 10, 
        backgroundColor: '#3C3839', 
        borderRadius: 10
    }, 

    input: {
        marginLeft: 20, 
        color: "white", 
        fontSize: 20, 
        flex: 1, 
        maxWidth: 275 

    }
})

