import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

export default class RoundButton extends  Component {
    render() {
        return (<TouchableOpacity 
                    style={[styles.container, {borderColor: this.props.borderColor}]}
                    onPress={this.props.onPress}>      
                <Text style={{color: "white", fontSize: 24}}>{this.props.title}</Text>          
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20, 
        paddingHorizontal: 60, 
        borderWidth: 2, 
        marginVertical: 10, 
        borderRadius: 40
    }
})

