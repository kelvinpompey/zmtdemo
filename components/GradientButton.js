import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo'; 

export default class RoundButton extends  Component {
    render() {
        return (
            <TouchableOpacity onPress = {this.props.onPress} style={{flex: 1}}>
                <LinearGradient 
                    style = {[styles.container, {borderColor: this.props.borderColor}]}
                    start={{x: 0, y: 0}} end={{x: 1, y: 1}}
                    locations={[0.25,0.75]}            
                    colors = {['#604984', '#9E5EB5']}>                          
                    <Text style = {styles.text}>{this.props.title}</Text>          
                </LinearGradient>
        </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        flex: 1, 
        paddingVertical: 20, 
        paddingHorizontal: 100, 
        marginTop: 20,
        borderRadius: 30, 
    }, 

    gradient: {
        height: 100
    }, 

    text: { 
        backgroundColor: 'transparent',
        color: "white", 
        fontSize: 24, 
        textAlign: "center"
    }
})